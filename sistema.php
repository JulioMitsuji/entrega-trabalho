<?php

//verifica sessão e nao permite entrar se nao estiver logado
session_start();
if((!isset($_SESSION['usuario']) == true) and (!isset($_SESSION['senha']) == true))
{
    unset($_SESSION['usuario']);
    unset($_SESSION['senha']);
    header('location: login.php');
}
$logado = $_SESSION['usuario'];




?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="sistema.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;800&display=swap" rel="stylesheet">
    <title>Sistema</title>
</head>
<body>
    <header>
        <nav>
            <h1 id="logo"> Bem-vindo <?php echo $logado ?></h1>

            <ul class="navlist">
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <aside>
            <h2> Cadastre seu pokémon </h2>
            <p> Cadastre todos os pokémons que você encontrar e mantenha sua pokédex atualizada </p>
            <form action="upload.php" method="post" enctype="multipart/form-data">
                <div class="textfield">
                    <label for="usuario">Nome</label>
                    <input type="text" name="Nome" required>
                </div>
                <div class="textfield">
                    <label for="tipo">Tipo</label>
                    <input type="text" name="Tipo" required>
                </div>
                <div class="textfield">
                    <label for="apelido">Apelido</label>
                    <input type="text" name="apelido" required>
                </div>
                <div class="textfield">
                    <label for="imagem">Imagem</label>
                    <input type="file" name="imagem" required>
                </div>
                <input type="submit" id="enviar" name="cadastrou" value="Cadastrar">
            </form>
        </aside>
        <div class="coluna">
            <h2> Seus pokémons</h2>
        </div>
    </main>
</body>
</html>